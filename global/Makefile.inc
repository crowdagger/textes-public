LATEX = latex
PDFLATEX = pdflatex
PSBOOK = psbook
PSNUP = psnup
PDF2PS = pdf2ps
USELESS_FILES = *~ html *.idv *.lg *.log *.aux *.ps *.pdf *.4ct *.xref *.dvi *.bb  *.4tc *.tmp

HTLATEX = htlatex
RM = rm -rf #gruik
CP = cp -f
LATEX_DIR = /usr/share/texmf/tex/latex/base

default: all

%.epub : *.md
	cat *.md | ./global/insecateur.py | pandoc -s --chapters -V --data-dir=global --epub-stylesheet=global/templates/epub.css -o $@  

%.odt : *.md
	cat *.md | ./global/insecateur.py | pandoc -s --chapters -V fontsize=10pt --data-dir=global -o $@ 

%.html : *.md
	cat *.md | ./global/insecateur.py | pandoc -t html5 -s --chapters -V -data-dir=global -o $@

%.pdf : *.md
	pandoc -s --chapters -V fontsize=10pt -V paper=a5 --data-dir=global -o $@  *.md










