Textes publics de Lizzie Crowdagger (dépôt git)
===============================================

Remarque préliminaire
---------------------

Si vous avez juste envie de lire mes textes finis, allez plutôt sur
http://crowdagger.fr ; vous en trouverez un certain nombre. Ceci est
le *repository* git public où je mets à disposition un certain nombre
de textes dans leur version « source », c'est-à-dire au format
*markdown*, dans une version pas toujours relue ni même, dans certains
cas, finis.

Mes objectifs à permettre cet accès public sont les suivants :

* permettre à des gens qui ont envie de suivre ce que j'écris au jour
  le jour de pouvoir le faire, du moins dans une certaine mesure (pour des
  raisons évidentes, je ne diffuserai pas les textes que je destine à
  envoyer à des éditeurs) ;
* le faire avec une plate-forme et des technologies libres (grâce à
Framasoft, en l'occurrence) ;
* diffuser également les fichiers « sources » d'un certain nombre de
textes finis, plutôt que juste les versions PDF et Epub.

J'ai bien conscience que cela n'intéressera pas la majorité des gens ;
si vous désirez juste avoir accès à la version définitive et mise en
page correctement de mes textes, je vous encourage à plutôt aller sur
mon site 
http://crowdagger.fr ou éventuellement dans des librairies.

L'aspect technique
------------------
Mes textes étant rédigés en *markdown*, il est plus ou moins possible de les
visualiser à la volée grâce à l'interface Web de GitLab, en suivant
les liens ci-dessous. Je précise *plus ou moins*, car je n'utilise pas
exactement le même logiciel pour la conversion, ce qui fait notamment
que le début de fichier affiche des caractères bizarres (des %) qui
devraient être interprêtées comme le titre et l'auteur.

Autrement dit : vous *pouvez* consulter les textes en ligne sans avoir à
taper une ligne de commande dans un terminal, mais c'est moche.

Si vous voulez avoir une version mise en page correctement, il vous
faudra donc :

* cloner le dépôt git ;
* disposer au minimum du logiciel [Pandoc](http://pandoc.org/) (mais
aussi de LaTeX si vous voulez générer les fichiers PDF) ;
* aller dans le répertoire adéquat, et génerer le fichier de sortie
  que vous voulez : `make texte.epub` (pour le format epub) ou `make
  texte.pdf` (pour le format pdf).

Encore une fois, j'ai bien conscience que ce genre de manœuvres est
« un peu » réservée aux *geeks*. Donc encore une fois, si vous n'y
comprenez rien, allez plutôt lire ces textes sur
[mon site](http://crowdagger.fr).



Contenu
-------

Voici les textes disponibles actuellement :

* [Une histoire pour enfants](hell_butches/morgue.md)
* [Réagir sans violence](hell_butches/sigkill.md)
* [Bain de soleil](hell_butches/soleil.md)
* [Vraie vampire](hell_butches/vraie_vampire.md) (en cours de
rédaction)

Licence
-------

Sauf mention contraire, les textes publiés dans ce dépôt ne sont
_pas_, *du tout*, sous licence libre. Vous n'avez, en particulier,
*pas* le droit de les rediffuser.

Certes, c'est classique, mais je préfère le préciser car le format
« dépôt git » pourrait laisser penser le contraire, mais non, ces
textes sont soumis à un *copyright* classique (sauf ceux qui disent
explicitement le contraire, évidemment).

Voilà pour le contenu des textes. Maintenant pour ce qui est de la
mise en page (c'est-à-dire tout ce qui rélève du répertoire `global`)
vous pouvez reprendre mes petits scripts persos (`insecateur.py` ou
les fichiers `Makefile`) ainsi que les *templates* pandoc un tout
petit peu modifiés sans souci. Si ce dépôt peut aider des écrivains
qui trouvent LibreOffice trop compliqué et qui auraient envie de se
mettre à la combinaison ultra simple qu'est Markdown + Pandoc +
Makefiles + Git + LaTeX (+ un script maison), j'en serais très
contente. Et si vous utilisez ce genre de solution mais que, vous,
vous avez une façon beaucoup plus simple de la mettre en œuvre,
n'hésitez pas à m'en faire part.


